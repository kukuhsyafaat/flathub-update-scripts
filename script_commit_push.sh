# SPDX-FileCopyrightText: 2021 Albert Astals Cid <aacid@kde.org>
# SPDX-License-Identifier: MIT

# echo "Make sure the commit is correct"
# exit

for a in `ls -d org.*`; do
    echo $a;
    cd $a;
    diff=`git diff`
    if [ "x$diff" != "x" ]; then
        git commit -a -m "Version update"
#         git commit -a -m "Update to KF 5.65"
        git push tsdgeos master:updated_version --force
        firefox https://github.com/tsdgeos/$a/pull/new/updated_version
    fi
    cd ..;
    sleep 1
done
