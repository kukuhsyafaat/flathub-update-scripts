# SPDX-FileCopyrightText: 2021 Albert Astals Cid <aacid@kde.org>
# SPDX-License-Identifier: MIT

for a in `ls -d org.*`; do
    echo $a;
    cd $a;
    git fetch --multiple tsdgeos upstream &> /dev/null
    git merge upstream/master
    cd ..;
done
