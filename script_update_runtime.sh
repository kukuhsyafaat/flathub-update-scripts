# SPDX-FileCopyrightText: 2021 Albert Astals Cid <aacid@kde.org>
# SPDX-License-Identifier: MIT

files=`find . -name org.kde.*.json`
for f in $files; do
    sed -i 's/"runtime-version": "5.14"/"runtime-version": "5.15"/g' $f
done
