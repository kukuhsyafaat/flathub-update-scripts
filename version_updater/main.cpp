// SPDX-FileCopyrightText: 2021 Albert Astals Cid <aacid@kde.org>
// SPDX-License-Identifier: MIT

#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QRegularExpression>

static QByteArray downloadAndSha(const QUrl &url)
{
    QNetworkAccessManager nam;

    const QUrl newUrl = QUrl(url);
    QNetworkRequest request(newUrl);
    request.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
    request.setAttribute(QNetworkRequest::RedirectPolicyAttribute, QNetworkRequest::UserVerifiedRedirectPolicy);

    QEventLoop loop;

    QUrl redirectUrl;
    QNetworkReply *reply = nam.get(request);
    QObject::connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    QObject::connect(reply, &QNetworkReply::redirected, [&loop, reply, &redirectUrl](const QUrl &url) {
        loop.quit();
        reply->deleteLater();
        redirectUrl = url;
    });
    loop.exec();

    if (!redirectUrl.isEmpty()) {
        return downloadAndSha(redirectUrl);
    }

    if (reply->error() != QNetworkReply::NoError) {
        qDebug() << "ERROR: accessing" << newUrl << "failed" << reply->error();
    }
    QCryptographicHash sha256(QCryptographicHash::Sha256);
    const QByteArray all = reply->readAll();
    sha256.addData(all);
    reply->deleteLater();
    return sha256.result().toHex();
}

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    const QRegularExpression urlRegExp(".*\"url\".*:.*\"(.*)\".*,");

    if (argc != 4) {
        qDebug() << "Wrong number of arguments. Pass the folder containing all the org.kde followed from the from and to version numbers";
        return -1;
    }

    const char *fromVersion = argv[2];
    const char *toVersion = argv[3];

    const QDir parentDir(argv[1]);
    if (!parentDir.exists()) {
        qDebug() << argv[1] << "is not a folder";
        return -2;
    }

    const QFileInfoList folders = parentDir.entryInfoList( { "org.kde.*" } );

    if (folders.isEmpty()) {
        qDebug() << argv[1] << "doesn't contain any org.kde.org folder";
        return -3;
    }

    for (const QFileInfo &folder : folders) {
        const QDir d = QDir(folder.absoluteFilePath());
        const QFileInfoList jsonFiles = d.entryInfoList( { d.dirName() + ".json" } );

        if (jsonFiles.count() == 1) {
            QFile f(jsonFiles[0].absoluteFilePath());
            if (!f.open(QIODevice::ReadOnly)) {
                qDebug() << "Couldn't open" << jsonFiles[0].absoluteFilePath() << "for reading";
                continue;
            }
            qDebug() << jsonFiles[0].absoluteFilePath();
            QByteArray output;
            // Can't use QJsonDocument, it'd reorder all the files
            // so do lame line parsing, this depends on "url": being followed by "sha256": lines
            while (!f.atEnd()) {
                QByteArray line = f.readLine();
                if (line.trimmed().startsWith("\"url\":")) {
                    const QRegularExpressionMatch oldUrlMatch = urlRegExp.match(line);
                    if (!oldUrlMatch.hasMatch()) {
                        qDebug() << "WARNING: url line didn't match regexp" << line;
                        continue;
                    }
                    const QByteArray oldLine = line;
                    line.replace(fromVersion, toVersion);
                    if (line != oldLine) {
                        output += line;

                        const QRegularExpressionMatch newUrlMatch = urlRegExp.match(line);
                        if (!newUrlMatch.hasMatch()) {
                            qDebug() << "WARNING: url line didn't match regexp" << line;
                            continue;
                        }

                        line = f.readLine();
                        if (!line.trimmed().startsWith("\"sha256\":")) {
                            qDebug() << "WARNING: url line wasn't followed by sha256 line" << line;
                            continue;
                        }

                        const QUrl newUrl = newUrlMatch.captured(1);
                        const QByteArray newSha = downloadAndSha(newUrl);
                        line = line.left(line.indexOf("\"sha256\":") + QString("\"sha256\":").length());
                        line = line + " \"" + newSha + "\"\n";
                    } else {
                        qDebug() << "\tNot updating" << oldUrlMatch.captured(1);
                    }
                }
                output += line;
            }
            f.close();

            if (!f.open(QIODevice::WriteOnly)) {
                qDebug() << "Couldn't open" << jsonFiles[0].absoluteFilePath() << "for writing";
                continue;
            }
            f.write(output);
            f.close();
        } else {
        }
    }
}
