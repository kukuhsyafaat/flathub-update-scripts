# SPDX-FileCopyrightText: 2021 Albert Astals Cid <aacid@kde.org>
# SPDX-License-Identifier: MIT

for a in `ls -d org.*`; do
    echo $a;
    cd $a;
    git diff
    cd ..;
done
